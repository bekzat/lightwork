import ChainIcon from './ChainIcon'
import RobotIcon from './RobotIcon'

export default {
  ChainIcon,
  RobotIcon
}
