import { createRouter, createWebHistory } from 'vue-router'
import SideMenu from '../layouts/side-menu/Main.vue'
import Dashboard from '../views/dashboard/Main.vue'
import QueueRuns from '../views/queue-runs/Main.vue'
import QueueHistory from '../views/queue-history/Main.vue'
import QueueCalendar from '../views/queue-calendar/Main.vue'
import QueueLog from '../views/queue-log/Main.vue'
import RobotsAll from '../views/robots-all/Main.vue'
import RobotsDeployment from '../views/robots-deployment/Main.vue'
import ProcessesAll from '../views/processes/Main.vue'
import ProcessLog from '../views/process-log/Main.vue'
import GroupsAll from '../views/groups-all/Main.vue'
import GroupsLog from '../views/groups-log/Main.vue'
import SettingsNWD from '../views/settings-nwd/Main.vue'
import SettingsSMTP from '../views/settings-smtp/Main.vue'
import SettingsDBPassword from '../views/settings-database-password/Main.vue'
import SettingsLicense from '../views/settings-license/Main.vue'
import SettingsOther from '../views/settings-other/Main.vue'
import Calendar from '../views/calendar/Main.vue'
import UsersAll from '../views/users-all/Main.vue'
import UsersDepartments from '../views/users-departments/Main.vue'
import UsersActivity from '../views/users-activity/Main.vue'
import UsersLoginHistory from '../views/users-login-history/Main.vue'
import UsersRoles from '../views/users-roles/Main.vue'
import WorkflowStorage from '../views/workflow-storage/Main.vue'
import Login from '../views/login/Main.vue'
import Register from '../views/register/Main.vue'
import ErrorPage from '../views/error-page/Main.vue'
import ChangePassword from '../views/change-password/Main.vue'
import RegularTable from '../views/regular-table/Main.vue'
import Tabulator from '../views/tabulator/Main.vue'
import Button from '../views/button/Main.vue'
import Modal from '../views/modal/Main.vue'
import SlideOver from '../views/slide-over/Main.vue'
import Alert from '../views/alert/Main.vue'
import Tooltip from '../views/tooltip/Main.vue'
import Dropdown from '../views/dropdown/Main.vue'
import RegularForm from '../views/regular-form/Main.vue'
import Datepicker from '../views/datepicker/Main.vue'
import TomSelect from '../views/tom-select/Main.vue'
import FileUpload from '../views/file-upload/Main.vue'
import Validation from '../views/validation/Main.vue'

const routes = [
  {
    path: '/',
    component: SideMenu,
    children: [
      {
        path: 'dashboard',
        name: 'side-menu-dashboard',
        component: Dashboard
      },
      {
        path: 'queue-runs',
        name: 'side-menu-queue-runs',
        component: QueueRuns
      },
      {
        path: 'queue-history',
        name: 'side-menu-queue-history',
        component: QueueHistory
      },
      {
        path: 'queue-calendar',
        name: 'side-menu-queue-calendar',
        component: QueueCalendar
      },
      {
        path: 'queue-log',
        name: 'side-menu-queue-log',
        component: QueueLog
      },
      {
        path: 'robots-all',
        name: 'side-menu-robots-all',
        component: RobotsAll
      },
      {
        path: 'robots-deployment',
        name: 'side-menu-robots-deployment',
        component: RobotsDeployment
      },
      {
        path: 'processes-all',
        name: 'side-menu-processes-all',
        component: ProcessesAll
      },
      {
        path: 'process-log',
        name: 'side-menu-process-log',
        component: ProcessLog
      },
      {
        path: 'groups-all',
        name: 'side-menu-groups-all',
        component: GroupsAll
      },
      {
        path: 'groups-log',
        name: 'side-menu-groups-log',
        component: GroupsLog
      },
      {
        path: 'calendar',
        name: 'side-menu-calendar',
        component: Calendar
      },
      {
        path: 'settings-smtp',
        name: 'side-menu-settings-smtp',
        component: SettingsSMTP
      },
      {
        path: 'settings-nwd',
        name: 'side-menu-settings-nwd',
        component: SettingsNWD
      },
      {
        path: 'settings-database-password',
        name: 'side-menu-settings-database-password',
        component: SettingsDBPassword
      },
      {
        path: 'settings-license',
        name: 'side-menu-settings-license',
        component: SettingsLicense
      },
      {
        path: 'settings-other',
        name: 'side-menu-settings-other',
        component: SettingsOther
      },
      {
        path: 'users-all',
        name: 'side-menu-users-all',
        component: UsersAll
      },
      {
        path: 'users-departments',
        name: 'side-menu-users-departments',
        component: UsersDepartments
      },
      {
        path: 'users-activity',
        name: 'side-menu-users-activity',
        component: UsersActivity
      },
      {
        path: 'users-login-history',
        name: 'side-menu-users-login-history',
        component: UsersLoginHistory
      },
      {
        path: 'users-roles',
        name: 'side-menu-users-roles',
        component: UsersRoles
      },
      {
        path: 'workflow-storage',
        name: 'side-menu-workflow-storage',
        component: WorkflowStorage
      },
      {
        path: 'change-password',
        name: 'side-menu-change-password',
        component: ChangePassword
      },
      {
        path: 'regular-table',
        name: 'side-menu-regular-table',
        component: RegularTable
      },
      {
        path: 'tabulator',
        name: 'side-menu-tabulator',
        component: Tabulator
      },
      {
        path: 'button',
        name: 'side-menu-button',
        component: Button
      },
      {
        path: 'modal',
        name: 'side-menu-modal',
        component: Modal
      },
      {
        path: 'slide-over',
        name: 'side-menu-slide-over',
        component: SlideOver
      },
      {
        path: 'alert',
        name: 'side-menu-alert',
        component: Alert
      },
      {
        path: 'tooltip',
        name: 'side-menu-tooltip',
        component: Tooltip
      },
      {
        path: 'dropdown',
        name: 'side-menu-dropdown',
        component: Dropdown
      },
      {
        path: 'regular-form',
        name: 'side-menu-regular-form',
        component: RegularForm
      },
      {
        path: 'datepicker',
        name: 'side-menu-datepicker',
        component: Datepicker
      },
      {
        path: 'tom-select',
        name: 'side-menu-tom-select',
        component: TomSelect
      },
      {
        path: 'file-upload',
        name: 'side-menu-file-upload',
        component: FileUpload
      },
      {
        path: 'validation',
        name: 'side-menu-validation',
        component: Validation
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/register',
    name: 'register',
    component: Register
  },
  {
    path: '/error-page',
    name: 'error-page',
    component: ErrorPage
  },
  {
    path: '/:pathMatch(.*)*',
    component: ErrorPage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { left: 0, top: 0 }
  }
})

export default router
