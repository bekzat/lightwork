/* eslint-disable */
import axios from 'axios'
import qs from 'qs'
let domain = ''
export const getDomain = () => {
  return domain
}
export const setDomain = ($domain) => {
  domain = $domain
}
export const request = (method, url, body, queryParameters, form, config) => {
  method = method.toLowerCase()
  let keys = Object.keys(queryParameters)
  let queryUrl = url
  if (keys.length > 0) {
    queryUrl = url + '?' + qs.stringify(queryParameters)
  }
  // let queryUrl = url+(keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
  if (body) {
    return axios[method](queryUrl, body, config)
  } else if (method === 'get') {
    return axios[method](queryUrl, config)
  } else {
    return axios[method](queryUrl, qs.stringify(form), config)
  }
}
/*==========================================================
 *                    
 ==========================================================*/
/**
 * 
 * request: GetById
 * url: GetByIdURL
 * method: GetById_TYPE
 * raw_url: GetById_RAW_URL
 * @param id - 
 */
export const GetById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetById_RAW_URL = function() {
  return '/api/GetById'
}
export const GetById_TYPE = function() {
  return 'get'
}
export const GetByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetByIdV2
 * url: GetByIdV2URL
 * method: GetByIdV2_TYPE
 * raw_url: GetByIdV2_RAW_URL
 * @param id - 
 */
export const GetByIdV2 = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetByIdV2'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetByIdV2_RAW_URL = function() {
  return '/api/GetByIdV2'
}
export const GetByIdV2_TYPE = function() {
  return 'get'
}
export const GetByIdV2URL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetByIdV2'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetSchedules
 * url: GetSchedulesURL
 * method: GetSchedules_TYPE
 * raw_url: GetSchedules_RAW_URL
 * @param custCode - 
 * @param scheduleName - 
 * @param processName - 
 * @param robotName - 
 * @param isActive - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 * @param isIncludeImmediately - 
 * @param isWeb - 
 */
export const GetSchedules = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetSchedules'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters['isIncludeImmediately'] !== undefined) {
    queryParameters['isIncludeImmediately'] = parameters['isIncludeImmediately']
  }
  if (parameters['isWeb'] !== undefined) {
    queryParameters['isWeb'] = parameters['isWeb']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetSchedules_RAW_URL = function() {
  return '/api/GetSchedules'
}
export const GetSchedules_TYPE = function() {
  return 'get'
}
export const GetSchedulesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetSchedules'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters['isIncludeImmediately'] !== undefined) {
    queryParameters['isIncludeImmediately'] = parameters['isIncludeImmediately']
  }
  if (parameters['isWeb'] !== undefined) {
    queryParameters['isWeb'] = parameters['isWeb']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetRunScheduleByRobotList
 * url: GetRunScheduleByRobotListURL
 * method: GetRunScheduleByRobotList_TYPE
 * raw_url: GetRunScheduleByRobotList_RAW_URL
 * @param robotList - 
 */
export const GetRunScheduleByRobotList = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetRunScheduleByRobotList'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['robotList'] !== undefined) {
    body = parameters['robotList']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const GetRunScheduleByRobotList_RAW_URL = function() {
  return '/api/GetRunScheduleByRobotList'
}
export const GetRunScheduleByRobotList_TYPE = function() {
  return 'post'
}
export const GetRunScheduleByRobotListURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetRunScheduleByRobotList'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetNextScheduleByNextScheduleId
 * url: GetNextScheduleByNextScheduleIdURL
 * method: GetNextScheduleByNextScheduleId_TYPE
 * raw_url: GetNextScheduleByNextScheduleId_RAW_URL
 * @param id - 
 * @param robotGuid - 
 */
export const GetNextScheduleByNextScheduleId = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetNextScheduleByNextScheduleId'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['robotGuid'] !== undefined) {
    queryParameters['robotGuid'] = parameters['robotGuid']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetNextScheduleByNextScheduleId_RAW_URL = function() {
  return '/api/GetNextScheduleByNextScheduleId'
}
export const GetNextScheduleByNextScheduleId_TYPE = function() {
  return 'get'
}
export const GetNextScheduleByNextScheduleIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetNextScheduleByNextScheduleId'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['robotGuid'] !== undefined) {
    queryParameters['robotGuid'] = parameters['robotGuid']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetRunImmediatelySchedules
 * url: GetRunImmediatelySchedulesURL
 * method: GetRunImmediatelySchedules_TYPE
 * raw_url: GetRunImmediatelySchedules_RAW_URL
 * @param custCode - 
 * @param scheduleId - 
 * @param scheduleName - 
 * @param userStart - 
 * @param userStop - 
 * @param isActive - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 * @param isIncludeImmediately - 
 */
export const GetRunImmediatelySchedules = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetRunImmediatelySchedules'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['userStart'] !== undefined) {
    queryParameters['userStart'] = parameters['userStart']
  }
  if (parameters['userStop'] !== undefined) {
    queryParameters['userStop'] = parameters['userStop']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters['isIncludeImmediately'] !== undefined) {
    queryParameters['isIncludeImmediately'] = parameters['isIncludeImmediately']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetRunImmediatelySchedules_RAW_URL = function() {
  return '/api/GetRunImmediatelySchedules'
}
export const GetRunImmediatelySchedules_TYPE = function() {
  return 'get'
}
export const GetRunImmediatelySchedulesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetRunImmediatelySchedules'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['userStart'] !== undefined) {
    queryParameters['userStart'] = parameters['userStart']
  }
  if (parameters['userStop'] !== undefined) {
    queryParameters['userStop'] = parameters['userStop']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters['isIncludeImmediately'] !== undefined) {
    queryParameters['isIncludeImmediately'] = parameters['isIncludeImmediately']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetScheduleCheckPoints
 * url: GetScheduleCheckPointsURL
 * method: GetScheduleCheckPoints_TYPE
 * raw_url: GetScheduleCheckPoints_RAW_URL
 * @param processName - 
 * @param isDynamicCheckpoint - 
 */
export const GetScheduleCheckPoints = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetScheduleCheckPoints'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['isDynamicCheckpoint'] !== undefined) {
    queryParameters['IsDynamicCheckpoint'] = parameters['isDynamicCheckpoint']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetScheduleCheckPoints_RAW_URL = function() {
  return '/api/GetScheduleCheckPoints'
}
export const GetScheduleCheckPoints_TYPE = function() {
  return 'get'
}
export const GetScheduleCheckPointsURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetScheduleCheckPoints'
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['isDynamicCheckpoint'] !== undefined) {
    queryParameters['IsDynamicCheckpoint'] = parameters['isDynamicCheckpoint']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetScheduleDateConfigs
 * url: GetScheduleDateConfigsURL
 * method: GetScheduleDateConfigs_TYPE
 * raw_url: GetScheduleDateConfigs_RAW_URL
 * @param id - 
 * @param custCode - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetScheduleDateConfigs = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetScheduleDateConfigs'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['custCode'] !== undefined) {
    queryParameters['CustCode'] = parameters['custCode']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetScheduleDateConfigs_RAW_URL = function() {
  return '/api/GetScheduleDateConfigs'
}
export const GetScheduleDateConfigs_TYPE = function() {
  return 'get'
}
export const GetScheduleDateConfigsURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetScheduleDateConfigs'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['custCode'] !== undefined) {
    queryParameters['CustCode'] = parameters['custCode']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddOrUpdateSchedule
 * url: AddOrUpdateScheduleURL
 * method: AddOrUpdateSchedule_TYPE
 * raw_url: AddOrUpdateSchedule_RAW_URL
 * @param request - 
 */
export const AddOrUpdateSchedule = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddOrUpdateSchedule'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddOrUpdateSchedule_RAW_URL = function() {
  return '/api/AddOrUpdateSchedule'
}
export const AddOrUpdateSchedule_TYPE = function() {
  return 'post'
}
export const AddOrUpdateScheduleURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddOrUpdateSchedule'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: SetScheduleStatus
 * url: SetScheduleStatusURL
 * method: SetScheduleStatus_TYPE
 * raw_url: SetScheduleStatus_RAW_URL
 * @param id - 
 * @param isActive - 
 */
export const SetScheduleStatus = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/SetScheduleStatus'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('patch', domain + path, body, queryParameters, form, config)
}
export const SetScheduleStatus_RAW_URL = function() {
  return '/api/SetScheduleStatus'
}
export const SetScheduleStatus_TYPE = function() {
  return 'patch'
}
export const SetScheduleStatusURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/SetScheduleStatus'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: SetScheduleTrigger
 * url: SetScheduleTriggerURL
 * method: SetScheduleTrigger_TYPE
 * raw_url: SetScheduleTrigger_RAW_URL
 * @param request - 
 */
export const SetScheduleTrigger = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/SetScheduleTrigger'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const SetScheduleTrigger_RAW_URL = function() {
  return '/api/SetScheduleTrigger'
}
export const SetScheduleTrigger_TYPE = function() {
  return 'post'
}
export const SetScheduleTriggerURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/SetScheduleTrigger'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: DeleteSchedule
 * url: DeleteScheduleURL
 * method: DeleteSchedule_TYPE
 * raw_url: DeleteSchedule_RAW_URL
 * @param id - 
 */
export const DeleteSchedule = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/DeleteSchedule'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const DeleteSchedule_RAW_URL = function() {
  return '/api/DeleteSchedule'
}
export const DeleteSchedule_TYPE = function() {
  return 'delete'
}
export const DeleteScheduleURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/DeleteSchedule'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetScheduleMonths
 * url: GetScheduleMonthsURL
 * method: GetScheduleMonths_TYPE
 * raw_url: GetScheduleMonths_RAW_URL
 * @param scheduleId - 
 */
export const GetScheduleMonths = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetScheduleMonths'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetScheduleMonths_RAW_URL = function() {
  return '/api/GetScheduleMonths'
}
export const GetScheduleMonths_TYPE = function() {
  return 'get'
}
export const GetScheduleMonthsURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetScheduleMonths'
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddScheduleMonth
 * url: AddScheduleMonthURL
 * method: AddScheduleMonth_TYPE
 * raw_url: AddScheduleMonth_RAW_URL
 * @param request - 
 */
export const AddScheduleMonth = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddScheduleMonth'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddScheduleMonth_RAW_URL = function() {
  return '/api/AddScheduleMonth'
}
export const AddScheduleMonth_TYPE = function() {
  return 'post'
}
export const AddScheduleMonthURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddScheduleMonth'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: DeleteRecentScheduleMonth
 * url: DeleteRecentScheduleMonthURL
 * method: DeleteRecentScheduleMonth_TYPE
 * raw_url: DeleteRecentScheduleMonth_RAW_URL
 * @param scheduleId - 
 */
export const DeleteRecentScheduleMonth = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/DeleteRecentScheduleMonth'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const DeleteRecentScheduleMonth_RAW_URL = function() {
  return '/api/DeleteRecentScheduleMonth'
}
export const DeleteRecentScheduleMonth_TYPE = function() {
  return 'delete'
}
export const DeleteRecentScheduleMonthURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/DeleteRecentScheduleMonth'
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetDashBoardEvent
 * url: GetDashBoardEventURL
 * method: GetDashBoardEvent_TYPE
 * raw_url: GetDashBoardEvent_RAW_URL
 * @param custCode - 
 * @param startDate - 
 * @param endDate - 
 */
export const GetDashBoardEvent = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetDashBoardEvent'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['startDate'] !== undefined) {
    queryParameters['startDate'] = parameters['startDate']
  }
  if (parameters['endDate'] !== undefined) {
    queryParameters['endDate'] = parameters['endDate']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetDashBoardEvent_RAW_URL = function() {
  return '/api/GetDashBoardEvent'
}
export const GetDashBoardEvent_TYPE = function() {
  return 'get'
}
export const GetDashBoardEventURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetDashBoardEvent'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['startDate'] !== undefined) {
    queryParameters['startDate'] = parameters['startDate']
  }
  if (parameters['endDate'] !== undefined) {
    queryParameters['endDate'] = parameters['endDate']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: SetForceEnd
 * url: SetForceEndURL
 * method: SetForceEnd_TYPE
 * raw_url: SetForceEnd_RAW_URL
 * @param id - 
 * @param isForceEnd - 
 * @param userId - 
 */
export const SetForceEnd = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/SetForceEnd'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['isForceEnd'] !== undefined) {
    queryParameters['isForceEnd'] = parameters['isForceEnd']
  }
  if (parameters['userId'] !== undefined) {
    queryParameters['userId'] = parameters['userId']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('patch', domain + path, body, queryParameters, form, config)
}
export const SetForceEnd_RAW_URL = function() {
  return '/api/SetForceEnd'
}
export const SetForceEnd_TYPE = function() {
  return 'patch'
}
export const SetForceEndURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/SetForceEnd'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['isForceEnd'] !== undefined) {
    queryParameters['isForceEnd'] = parameters['isForceEnd']
  }
  if (parameters['userId'] !== undefined) {
    queryParameters['userId'] = parameters['userId']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetScheduleForceEnd
 * url: GetScheduleForceEndURL
 * method: GetScheduleForceEnd_TYPE
 * raw_url: GetScheduleForceEnd_RAW_URL
 * @param custCode - 
 */
export const GetScheduleForceEnd = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetScheduleForceEnd'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetScheduleForceEnd_RAW_URL = function() {
  return '/api/GetScheduleForceEnd'
}
export const GetScheduleForceEnd_TYPE = function() {
  return 'get'
}
export const GetScheduleForceEndURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetScheduleForceEnd'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetHistoryById
 * url: GetHistoryByIdURL
 * method: GetHistoryById_TYPE
 * raw_url: GetHistoryById_RAW_URL
 * @param id - 
 */
export const GetHistoryById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetHistoryById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetHistoryById_RAW_URL = function() {
  return '/api/GetHistoryById'
}
export const GetHistoryById_TYPE = function() {
  return 'get'
}
export const GetHistoryByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetHistoryById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetSchedulesHistory
 * url: GetSchedulesHistoryURL
 * method: GetSchedulesHistory_TYPE
 * raw_url: GetSchedulesHistory_RAW_URL
 * @param custCode - 
 * @param scheduleId - 
 * @param scheduleName - 
 * @param modifiedByUser - 
 * @param modifiedByIp - 
 * @param modifiedByComputerName - 
 * @param modificationDetail - 
 * @param modifiedFrom - 
 * @param modifiedTo - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetSchedulesHistory = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetSchedulesHistory'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['modifiedByUser'] !== undefined) {
    queryParameters['modifiedByUser'] = parameters['modifiedByUser']
  }
  if (parameters['modifiedByIp'] !== undefined) {
    queryParameters['modifiedByIP'] = parameters['modifiedByIp']
  }
  if (parameters['modifiedByComputerName'] !== undefined) {
    queryParameters['modifiedByComputerName'] = parameters['modifiedByComputerName']
  }
  if (parameters['modificationDetail'] !== undefined) {
    queryParameters['modificationDetail'] = parameters['modificationDetail']
  }
  if (parameters['modifiedFrom'] !== undefined) {
    queryParameters['modifiedFrom'] = parameters['modifiedFrom']
  }
  if (parameters['modifiedTo'] !== undefined) {
    queryParameters['modifiedTo'] = parameters['modifiedTo']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetSchedulesHistory_RAW_URL = function() {
  return '/api/GetSchedulesHistory'
}
export const GetSchedulesHistory_TYPE = function() {
  return 'get'
}
export const GetSchedulesHistoryURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetSchedulesHistory'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['modifiedByUser'] !== undefined) {
    queryParameters['modifiedByUser'] = parameters['modifiedByUser']
  }
  if (parameters['modifiedByIp'] !== undefined) {
    queryParameters['modifiedByIP'] = parameters['modifiedByIp']
  }
  if (parameters['modifiedByComputerName'] !== undefined) {
    queryParameters['modifiedByComputerName'] = parameters['modifiedByComputerName']
  }
  if (parameters['modificationDetail'] !== undefined) {
    queryParameters['modificationDetail'] = parameters['modificationDetail']
  }
  if (parameters['modifiedFrom'] !== undefined) {
    queryParameters['modifiedFrom'] = parameters['modifiedFrom']
  }
  if (parameters['modifiedTo'] !== undefined) {
    queryParameters['modifiedTo'] = parameters['modifiedTo']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}