/* eslint-disable */
import axios from 'axios'
import qs from 'qs'
let domain = ''
export const getDomain = () => {
  return domain
}
export const setDomain = ($domain) => {
  domain = $domain
}
export const request = (method, url, body, queryParameters, form, config) => {
  method = method.toLowerCase()
  let keys = Object.keys(queryParameters)
  let queryUrl = url
  if (keys.length > 0) {
    queryUrl = url + '?' + qs.stringify(queryParameters)
  }
  // let queryUrl = url+(keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
  if (body) {
    return axios[method](queryUrl, body, config)
  } else if (method === 'get') {
    return axios[method](queryUrl, config)
  } else {
    return axios[method](queryUrl, qs.stringify(form), config)
  }
}
/*==========================================================
 *                    
 ==========================================================*/
/**
 * 
 * request: GetJobQueues
 * url: GetJobQueuesURL
 * method: GetJobQueues_TYPE
 * raw_url: GetJobQueues_RAW_URL
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetJobQueues = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetJobQueues'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetJobQueues_RAW_URL = function() {
  return '/api/GetJobQueues'
}
export const GetJobQueues_TYPE = function() {
  return 'get'
}
export const GetJobQueuesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetJobQueues'
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetJobQueuesHistory
 * url: GetJobQueuesHistoryURL
 * method: GetJobQueuesHistory_TYPE
 * raw_url: GetJobQueuesHistory_RAW_URL
 * @param scheduleName - 
 * @param robotName - 
 * @param dateStart - 
 * @param dateEnd - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetJobQueuesHistory = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetJobQueuesHistory'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['dateStart'] !== undefined) {
    queryParameters['dateStart'] = parameters['dateStart']
  }
  if (parameters['dateEnd'] !== undefined) {
    queryParameters['dateEnd'] = parameters['dateEnd']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetJobQueuesHistory_RAW_URL = function() {
  return '/api/GetJobQueuesHistory'
}
export const GetJobQueuesHistory_TYPE = function() {
  return 'get'
}
export const GetJobQueuesHistoryURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetJobQueuesHistory'
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['dateStart'] !== undefined) {
    queryParameters['dateStart'] = parameters['dateStart']
  }
  if (parameters['dateEnd'] !== undefined) {
    queryParameters['dateEnd'] = parameters['dateEnd']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddJobQueue
 * url: AddJobQueueURL
 * method: AddJobQueue_TYPE
 * raw_url: AddJobQueue_RAW_URL
 * @param request - 
 */
export const AddJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddJobQueue_RAW_URL = function() {
  return '/api/AddJobQueue'
}
export const AddJobQueue_TYPE = function() {
  return 'post'
}
export const AddJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddJobQueue'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: RemoveJobQueue
 * url: RemoveJobQueueURL
 * method: RemoveJobQueue_TYPE
 * raw_url: RemoveJobQueue_RAW_URL
 * @param id - 
 */
export const RemoveJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/RemoveJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const RemoveJobQueue_RAW_URL = function() {
  return '/api/RemoveJobQueue'
}
export const RemoveJobQueue_TYPE = function() {
  return 'delete'
}
export const RemoveJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/RemoveJobQueue'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: ForceStopJobQueue
 * url: ForceStopJobQueueURL
 * method: ForceStopJobQueue_TYPE
 * raw_url: ForceStopJobQueue_RAW_URL
 * @param id - 
 */
export const ForceStopJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/ForceStopJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const ForceStopJobQueue_RAW_URL = function() {
  return '/api/ForceStopJobQueue'
}
export const ForceStopJobQueue_TYPE = function() {
  return 'delete'
}
export const ForceStopJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/ForceStopJobQueue'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: TriggerJobQueues
 * url: TriggerJobQueuesURL
 * method: TriggerJobQueues_TYPE
 * raw_url: TriggerJobQueues_RAW_URL
 */
export const TriggerJobQueues = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/TriggerJobQueues'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const TriggerJobQueues_RAW_URL = function() {
  return '/api/TriggerJobQueues'
}
export const TriggerJobQueues_TYPE = function() {
  return 'get'
}
export const TriggerJobQueuesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/TriggerJobQueues'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: RunJobQueue
 * url: RunJobQueueURL
 * method: RunJobQueue_TYPE
 * raw_url: RunJobQueue_RAW_URL
 * @param request - 
 */
export const RunJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/RunJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('put', domain + path, body, queryParameters, form, config)
}
export const RunJobQueue_RAW_URL = function() {
  return '/api/RunJobQueue'
}
export const RunJobQueue_TYPE = function() {
  return 'put'
}
export const RunJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/RunJobQueue'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: UpdNextScheduleTimeAndUpdRunJobQueue
 * url: UpdNextScheduleTimeAndUpdRunJobQueueURL
 * method: UpdNextScheduleTimeAndUpdRunJobQueue_TYPE
 * raw_url: UpdNextScheduleTimeAndUpdRunJobQueue_RAW_URL
 * @param request - 
 */
export const UpdNextScheduleTimeAndUpdRunJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/UpdNextScheduleTimeAndUpdRunJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('put', domain + path, body, queryParameters, form, config)
}
export const UpdNextScheduleTimeAndUpdRunJobQueue_RAW_URL = function() {
  return '/api/UpdNextScheduleTimeAndUpdRunJobQueue'
}
export const UpdNextScheduleTimeAndUpdRunJobQueue_TYPE = function() {
  return 'put'
}
export const UpdNextScheduleTimeAndUpdRunJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/UpdNextScheduleTimeAndUpdRunJobQueue'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: StopJobQueue
 * url: StopJobQueueURL
 * method: StopJobQueue_TYPE
 * raw_url: StopJobQueue_RAW_URL
 * @param request - 
 */
export const StopJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/StopJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('put', domain + path, body, queryParameters, form, config)
}
export const StopJobQueue_RAW_URL = function() {
  return '/api/StopJobQueue'
}
export const StopJobQueue_TYPE = function() {
  return 'put'
}
export const StopJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/StopJobQueue'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: StopingJobQueue
 * url: StopingJobQueueURL
 * method: StopingJobQueue_TYPE
 * raw_url: StopingJobQueue_RAW_URL
 * @param request - 
 */
export const StopingJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/StopingJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('put', domain + path, body, queryParameters, form, config)
}
export const StopingJobQueue_RAW_URL = function() {
  return '/api/StopingJobQueue'
}
export const StopingJobQueue_TYPE = function() {
  return 'put'
}
export const StopingJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/StopingJobQueue'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: CompleteJobQueue
 * url: CompleteJobQueueURL
 * method: CompleteJobQueue_TYPE
 * raw_url: CompleteJobQueue_RAW_URL
 * @param request - 
 */
export const CompleteJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/CompleteJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('put', domain + path, body, queryParameters, form, config)
}
export const CompleteJobQueue_RAW_URL = function() {
  return '/api/CompleteJobQueue'
}
export const CompleteJobQueue_TYPE = function() {
  return 'put'
}
export const CompleteJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/CompleteJobQueue'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: ErrorJobQueue
 * url: ErrorJobQueueURL
 * method: ErrorJobQueue_TYPE
 * raw_url: ErrorJobQueue_RAW_URL
 * @param request - 
 */
export const ErrorJobQueue = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/ErrorJobQueue'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('put', domain + path, body, queryParameters, form, config)
}
export const ErrorJobQueue_RAW_URL = function() {
  return '/api/ErrorJobQueue'
}
export const ErrorJobQueue_TYPE = function() {
  return 'put'
}
export const ErrorJobQueueURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/ErrorJobQueue'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetJobQueuesTemp
 * url: GetJobQueuesTempURL
 * method: GetJobQueuesTemp_TYPE
 * raw_url: GetJobQueuesTemp_RAW_URL
 */
export const GetJobQueuesTemp = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetJobQueuesTemp'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetJobQueuesTemp_RAW_URL = function() {
  return '/api/GetJobQueuesTemp'
}
export const GetJobQueuesTemp_TYPE = function() {
  return 'get'
}
export const GetJobQueuesTempURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetJobQueuesTemp'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetRobotCommand
 * url: GetRobotCommandURL
 * method: GetRobotCommand_TYPE
 * raw_url: GetRobotCommand_RAW_URL
 */
export const GetRobotCommand = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetRobotCommand'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetRobotCommand_RAW_URL = function() {
  return '/api/GetRobotCommand'
}
export const GetRobotCommand_TYPE = function() {
  return 'get'
}
export const GetRobotCommandURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetRobotCommand'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddJobQueueByProcess
 * url: AddJobQueueByProcessURL
 * method: AddJobQueueByProcess_TYPE
 * raw_url: AddJobQueueByProcess_RAW_URL
 * @param request - 
 */
export const AddJobQueueByProcess = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddJobQueueByProcess'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddJobQueueByProcess_RAW_URL = function() {
  return '/api/AddJobQueueByProcess'
}
export const AddJobQueueByProcess_TYPE = function() {
  return 'post'
}
export const AddJobQueueByProcessURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddJobQueueByProcess'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}