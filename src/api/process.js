/* eslint-disable */
import axios from 'axios'
import qs from 'qs'
let domain = ''
export const getDomain = () => {
  return domain
}
export const setDomain = ($domain) => {
  domain = $domain
}
export const request = (method, url, body, queryParameters, form, config) => {
  method = method.toLowerCase()
  let keys = Object.keys(queryParameters)
  let queryUrl = url
  if (keys.length > 0) {
    queryUrl = url + '?' + qs.stringify(queryParameters)
  }
  // let queryUrl = url+(keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
  if (body) {
    return axios[method](queryUrl, body, config)
  } else if (method === 'get') {
    return axios[method](queryUrl, config)
  } else {
    return axios[method](queryUrl, qs.stringify(form), config)
  }
}
/*==========================================================
 *                    
 ==========================================================*/
/**
 * 
 * request: GetById
 * url: GetByIdURL
 * method: GetById_TYPE
 * raw_url: GetById_RAW_URL
 * @param id - 
 */
export const GetById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetById_RAW_URL = function() {
  return '/api/GetById'
}
export const GetById_TYPE = function() {
  return 'get'
}
export const GetByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetByIdV2
 * url: GetByIdV2URL
 * method: GetByIdV2_TYPE
 * raw_url: GetByIdV2_RAW_URL
 * @param id - 
 */
export const GetByIdV2 = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetByIdV2'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetByIdV2_RAW_URL = function() {
  return '/api/GetByIdV2'
}
export const GetByIdV2_TYPE = function() {
  return 'get'
}
export const GetByIdV2URL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetByIdV2'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetDepartmentEmailById
 * url: GetDepartmentEmailByIdURL
 * method: GetDepartmentEmailById_TYPE
 * raw_url: GetDepartmentEmailById_RAW_URL
 * @param id - 
 */
export const GetDepartmentEmailById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetDepartmentEmailById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetDepartmentEmailById_RAW_URL = function() {
  return '/api/GetDepartmentEmailById'
}
export const GetDepartmentEmailById_TYPE = function() {
  return 'get'
}
export const GetDepartmentEmailByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetDepartmentEmailById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetProcesses
 * url: GetProcessesURL
 * method: GetProcesses_TYPE
 * raw_url: GetProcesses_RAW_URL
 * @param processName - 
 * @param isActive - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetProcesses = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetProcesses'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetProcesses_RAW_URL = function() {
  return '/api/GetProcesses'
}
export const GetProcesses_TYPE = function() {
  return 'get'
}
export const GetProcessesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetProcesses'
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetFileProcessesById
 * url: GetFileProcessesByIdURL
 * method: GetFileProcessesById_TYPE
 * raw_url: GetFileProcessesById_RAW_URL
 * @param id - 
 */
export const GetFileProcessesById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetFileProcessesById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetFileProcessesById_RAW_URL = function() {
  return '/api/GetFileProcessesById'
}
export const GetFileProcessesById_TYPE = function() {
  return 'get'
}
export const GetFileProcessesByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetFileProcessesById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddOrUpdateProcess
 * url: AddOrUpdateProcessURL
 * method: AddOrUpdateProcess_TYPE
 * raw_url: AddOrUpdateProcess_RAW_URL
 * @param request - 
 */
export const AddOrUpdateProcess = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddOrUpdateProcess'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddOrUpdateProcess_RAW_URL = function() {
  return '/api/AddOrUpdateProcess'
}
export const AddOrUpdateProcess_TYPE = function() {
  return 'post'
}
export const AddOrUpdateProcessURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddOrUpdateProcess'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: ExecuteProcess
 * url: ExecuteProcessURL
 * method: ExecuteProcess_TYPE
 * raw_url: ExecuteProcess_RAW_URL
 * @param request - 
 */
export const ExecuteProcess = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/ExecuteProcess'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const ExecuteProcess_RAW_URL = function() {
  return '/api/ExecuteProcess'
}
export const ExecuteProcess_TYPE = function() {
  return 'post'
}
export const ExecuteProcessURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/ExecuteProcess'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: DeleteProcess
 * url: DeleteProcessURL
 * method: DeleteProcess_TYPE
 * raw_url: DeleteProcess_RAW_URL
 * @param id - 
 */
export const DeleteProcess = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/DeleteProcess'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const DeleteProcess_RAW_URL = function() {
  return '/api/DeleteProcess'
}
export const DeleteProcess_TYPE = function() {
  return 'delete'
}
export const DeleteProcessURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/DeleteProcess'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetHistoryById
 * url: GetHistoryByIdURL
 * method: GetHistoryById_TYPE
 * raw_url: GetHistoryById_RAW_URL
 * @param id - 
 */
export const GetHistoryById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetHistoryById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetHistoryById_RAW_URL = function() {
  return '/api/GetHistoryById'
}
export const GetHistoryById_TYPE = function() {
  return 'get'
}
export const GetHistoryByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetHistoryById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetProcessesHistory
 * url: GetProcessesHistoryURL
 * method: GetProcessesHistory_TYPE
 * raw_url: GetProcessesHistory_RAW_URL
 * @param custCode - 
 * @param processId - 
 * @param processName - 
 * @param modifiedByUser - 
 * @param modifiedByIp - 
 * @param modifiedByComputerName - 
 * @param modificationDetail - 
 * @param modifiedFrom - 
 * @param modifiedTo - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetProcessesHistory = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetProcessesHistory'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['processId'] !== undefined) {
    queryParameters['processId'] = parameters['processId']
  }
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['modifiedByUser'] !== undefined) {
    queryParameters['modifiedByUser'] = parameters['modifiedByUser']
  }
  if (parameters['modifiedByIp'] !== undefined) {
    queryParameters['modifiedByIP'] = parameters['modifiedByIp']
  }
  if (parameters['modifiedByComputerName'] !== undefined) {
    queryParameters['modifiedByComputerName'] = parameters['modifiedByComputerName']
  }
  if (parameters['modificationDetail'] !== undefined) {
    queryParameters['modificationDetail'] = parameters['modificationDetail']
  }
  if (parameters['modifiedFrom'] !== undefined) {
    queryParameters['modifiedFrom'] = parameters['modifiedFrom']
  }
  if (parameters['modifiedTo'] !== undefined) {
    queryParameters['modifiedTo'] = parameters['modifiedTo']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetProcessesHistory_RAW_URL = function() {
  return '/api/GetProcessesHistory'
}
export const GetProcessesHistory_TYPE = function() {
  return 'get'
}
export const GetProcessesHistoryURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetProcessesHistory'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['processId'] !== undefined) {
    queryParameters['processId'] = parameters['processId']
  }
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['modifiedByUser'] !== undefined) {
    queryParameters['modifiedByUser'] = parameters['modifiedByUser']
  }
  if (parameters['modifiedByIp'] !== undefined) {
    queryParameters['modifiedByIP'] = parameters['modifiedByIp']
  }
  if (parameters['modifiedByComputerName'] !== undefined) {
    queryParameters['modifiedByComputerName'] = parameters['modifiedByComputerName']
  }
  if (parameters['modificationDetail'] !== undefined) {
    queryParameters['modificationDetail'] = parameters['modificationDetail']
  }
  if (parameters['modifiedFrom'] !== undefined) {
    queryParameters['modifiedFrom'] = parameters['modifiedFrom']
  }
  if (parameters['modifiedTo'] !== undefined) {
    queryParameters['modifiedTo'] = parameters['modifiedTo']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}