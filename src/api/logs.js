/* eslint-disable */
import axios from 'axios'
import qs from 'qs'
let domain = ''
export const getDomain = () => {
  return domain
}
export const setDomain = ($domain) => {
  domain = $domain
}
export const request = (method, url, body, queryParameters, form, config) => {
  method = method.toLowerCase()
  let keys = Object.keys(queryParameters)
  let queryUrl = url
  if (keys.length > 0) {
    queryUrl = url + '?' + qs.stringify(queryParameters)
  }
  // let queryUrl = url+(keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
  if (body) {
    return axios[method](queryUrl, body, config)
  } else if (method === 'get') {
    return axios[method](queryUrl, config)
  } else {
    return axios[method](queryUrl, qs.stringify(form), config)
  }
}
/*==========================================================
 *                    
 ==========================================================*/
/**
 * 
 * request: GetLogScheduleById
 * url: GetLogScheduleByIdURL
 * method: GetLogScheduleById_TYPE
 * raw_url: GetLogScheduleById_RAW_URL
 * @param id - 
 */
export const GetLogScheduleById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogScheduleById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogScheduleById_RAW_URL = function() {
  return '/api/GetLogScheduleById'
}
export const GetLogScheduleById_TYPE = function() {
  return 'get'
}
export const GetLogScheduleByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogScheduleById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogSchedules
 * url: GetLogSchedulesURL
 * method: GetLogSchedules_TYPE
 * raw_url: GetLogSchedules_RAW_URL
 * @param dateFrom - 
 * @param dateTo - 
 * @param logTypeId - 
 * @param statusId - 
 * @param scheduleName - 
 * @param statusDesc - 
 * @param logTypeDesc - 
 * @param message - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 * @param isTestMode - 
 */
export const GetLogSchedules = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogSchedules'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['dateFrom'] !== undefined) {
    queryParameters['dateFrom'] = parameters['dateFrom']
  }
  if (parameters['dateTo'] !== undefined) {
    queryParameters['dateTo'] = parameters['dateTo']
  }
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['statusId'] !== undefined) {
    queryParameters['statusId'] = parameters['statusId']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['statusDesc'] !== undefined) {
    queryParameters['statusDesc'] = parameters['statusDesc']
  }
  if (parameters['logTypeDesc'] !== undefined) {
    queryParameters['logTypeDesc'] = parameters['logTypeDesc']
  }
  if (parameters['message'] !== undefined) {
    queryParameters['message'] = parameters['message']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters['isTestMode'] !== undefined) {
    queryParameters['isTestMode'] = parameters['isTestMode']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogSchedules_RAW_URL = function() {
  return '/api/GetLogSchedules'
}
export const GetLogSchedules_TYPE = function() {
  return 'get'
}
export const GetLogSchedulesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogSchedules'
  if (parameters['dateFrom'] !== undefined) {
    queryParameters['dateFrom'] = parameters['dateFrom']
  }
  if (parameters['dateTo'] !== undefined) {
    queryParameters['dateTo'] = parameters['dateTo']
  }
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['statusId'] !== undefined) {
    queryParameters['statusId'] = parameters['statusId']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['statusDesc'] !== undefined) {
    queryParameters['statusDesc'] = parameters['statusDesc']
  }
  if (parameters['logTypeDesc'] !== undefined) {
    queryParameters['logTypeDesc'] = parameters['logTypeDesc']
  }
  if (parameters['message'] !== undefined) {
    queryParameters['message'] = parameters['message']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters['isTestMode'] !== undefined) {
    queryParameters['isTestMode'] = parameters['isTestMode']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogSchedule
 * url: AddLogScheduleURL
 * method: AddLogSchedule_TYPE
 * raw_url: AddLogSchedule_RAW_URL
 * @param request - 
 */
export const AddLogSchedule = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogSchedule'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogSchedule_RAW_URL = function() {
  return '/api/AddLogSchedule'
}
export const AddLogSchedule_TYPE = function() {
  return 'post'
}
export const AddLogScheduleURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogSchedule'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogSchedules
 * url: AddLogSchedulesURL
 * method: AddLogSchedules_TYPE
 * raw_url: AddLogSchedules_RAW_URL
 * @param request - 
 */
export const AddLogSchedules = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogSchedules'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogSchedules_RAW_URL = function() {
  return '/api/AddLogSchedules'
}
export const AddLogSchedules_TYPE = function() {
  return 'post'
}
export const AddLogSchedulesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogSchedules'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogProcessById
 * url: GetLogProcessByIdURL
 * method: GetLogProcessById_TYPE
 * raw_url: GetLogProcessById_RAW_URL
 * @param id - 
 */
export const GetLogProcessById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogProcessById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogProcessById_RAW_URL = function() {
  return '/api/GetLogProcessById'
}
export const GetLogProcessById_TYPE = function() {
  return 'get'
}
export const GetLogProcessByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogProcessById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogProcesses
 * url: GetLogProcessesURL
 * method: GetLogProcesses_TYPE
 * raw_url: GetLogProcesses_RAW_URL
 * @param dateFrom - 
 * @param dateTo - 
 * @param jobQueueId - 
 * @param logTypeId - 
 * @param statusId - 
 * @param processId - 
 * @param scheduleId - 
 * @param processName - 
 * @param scheduleName - 
 * @param statusDesc - 
 * @param logTypeDesc - 
 * @param message - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 * @param isTestMode - 
 */
export const GetLogProcesses = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogProcesses'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['dateFrom'] !== undefined) {
    queryParameters['dateFrom'] = parameters['dateFrom']
  }
  if (parameters['dateTo'] !== undefined) {
    queryParameters['dateTo'] = parameters['dateTo']
  }
  if (parameters['jobQueueId'] !== undefined) {
    queryParameters['JobQueueId'] = parameters['jobQueueId']
  }
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['statusId'] !== undefined) {
    queryParameters['statusId'] = parameters['statusId']
  }
  if (parameters['processId'] !== undefined) {
    queryParameters['processId'] = parameters['processId']
  }
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['statusDesc'] !== undefined) {
    queryParameters['statusDesc'] = parameters['statusDesc']
  }
  if (parameters['logTypeDesc'] !== undefined) {
    queryParameters['logTypeDesc'] = parameters['logTypeDesc']
  }
  if (parameters['message'] !== undefined) {
    queryParameters['message'] = parameters['message']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters['isTestMode'] !== undefined) {
    queryParameters['isTestMode'] = parameters['isTestMode']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogProcesses_RAW_URL = function() {
  return '/api/GetLogProcesses'
}
export const GetLogProcesses_TYPE = function() {
  return 'get'
}
export const GetLogProcessesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogProcesses'
  if (parameters['dateFrom'] !== undefined) {
    queryParameters['dateFrom'] = parameters['dateFrom']
  }
  if (parameters['dateTo'] !== undefined) {
    queryParameters['dateTo'] = parameters['dateTo']
  }
  if (parameters['jobQueueId'] !== undefined) {
    queryParameters['JobQueueId'] = parameters['jobQueueId']
  }
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['statusId'] !== undefined) {
    queryParameters['statusId'] = parameters['statusId']
  }
  if (parameters['processId'] !== undefined) {
    queryParameters['processId'] = parameters['processId']
  }
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters['processName'] !== undefined) {
    queryParameters['processName'] = parameters['processName']
  }
  if (parameters['scheduleName'] !== undefined) {
    queryParameters['scheduleName'] = parameters['scheduleName']
  }
  if (parameters['statusDesc'] !== undefined) {
    queryParameters['statusDesc'] = parameters['statusDesc']
  }
  if (parameters['logTypeDesc'] !== undefined) {
    queryParameters['logTypeDesc'] = parameters['logTypeDesc']
  }
  if (parameters['message'] !== undefined) {
    queryParameters['message'] = parameters['message']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters['isTestMode'] !== undefined) {
    queryParameters['isTestMode'] = parameters['isTestMode']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogProcess
 * url: AddLogProcessURL
 * method: AddLogProcess_TYPE
 * raw_url: AddLogProcess_RAW_URL
 * @param request - 
 */
export const AddLogProcess = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogProcess'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogProcess_RAW_URL = function() {
  return '/api/AddLogProcess'
}
export const AddLogProcess_TYPE = function() {
  return 'post'
}
export const AddLogProcessURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogProcess'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogProcesses
 * url: AddLogProcessesURL
 * method: AddLogProcesses_TYPE
 * raw_url: AddLogProcesses_RAW_URL
 * @param request - 
 */
export const AddLogProcesses = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogProcesses'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogProcesses_RAW_URL = function() {
  return '/api/AddLogProcesses'
}
export const AddLogProcesses_TYPE = function() {
  return 'post'
}
export const AddLogProcessesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogProcesses'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogRobots
 * url: GetLogRobotsURL
 * method: GetLogRobots_TYPE
 * raw_url: GetLogRobots_RAW_URL
 * @param logTypeId - 
 * @param statusId - 
 * @param robotName - 
 * @param statusDesc - 
 * @param logTypeDesc - 
 * @param message - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetLogRobots = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogRobots'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['statusId'] !== undefined) {
    queryParameters['statusId'] = parameters['statusId']
  }
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['statusDesc'] !== undefined) {
    queryParameters['statusDesc'] = parameters['statusDesc']
  }
  if (parameters['logTypeDesc'] !== undefined) {
    queryParameters['logTypeDesc'] = parameters['logTypeDesc']
  }
  if (parameters['message'] !== undefined) {
    queryParameters['message'] = parameters['message']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogRobots_RAW_URL = function() {
  return '/api/GetLogRobots'
}
export const GetLogRobots_TYPE = function() {
  return 'get'
}
export const GetLogRobotsURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogRobots'
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['statusId'] !== undefined) {
    queryParameters['statusId'] = parameters['statusId']
  }
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['statusDesc'] !== undefined) {
    queryParameters['statusDesc'] = parameters['statusDesc']
  }
  if (parameters['logTypeDesc'] !== undefined) {
    queryParameters['logTypeDesc'] = parameters['logTypeDesc']
  }
  if (parameters['message'] !== undefined) {
    queryParameters['message'] = parameters['message']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogRobot
 * url: AddLogRobotURL
 * method: AddLogRobot_TYPE
 * raw_url: AddLogRobot_RAW_URL
 * @param request - 
 */
export const AddLogRobot = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogRobot'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogRobot_RAW_URL = function() {
  return '/api/AddLogRobot'
}
export const AddLogRobot_TYPE = function() {
  return 'post'
}
export const AddLogRobotURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogRobot'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogRobots
 * url: AddLogRobotsURL
 * method: AddLogRobots_TYPE
 * raw_url: AddLogRobots_RAW_URL
 * @param request - 
 */
export const AddLogRobots = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogRobots'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogRobots_RAW_URL = function() {
  return '/api/AddLogRobots'
}
export const AddLogRobots_TYPE = function() {
  return 'post'
}
export const AddLogRobotsURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogRobots'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogRobotCommand
 * url: GetLogRobotCommandURL
 * method: GetLogRobotCommand_TYPE
 * raw_url: GetLogRobotCommand_RAW_URL
 * @param robotId - 
 * @param robotName - 
 * @param robotGuid - 
 * @param commandId - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetLogRobotCommand = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogRobotCommand'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['robotId'] !== undefined) {
    queryParameters['robotId'] = parameters['robotId']
  }
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['robotGuid'] !== undefined) {
    queryParameters['robotGuid'] = parameters['robotGuid']
  }
  if (parameters['commandId'] !== undefined) {
    queryParameters['commandId'] = parameters['commandId']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogRobotCommand_RAW_URL = function() {
  return '/api/GetLogRobotCommand'
}
export const GetLogRobotCommand_TYPE = function() {
  return 'get'
}
export const GetLogRobotCommandURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogRobotCommand'
  if (parameters['robotId'] !== undefined) {
    queryParameters['robotId'] = parameters['robotId']
  }
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['robotGuid'] !== undefined) {
    queryParameters['robotGuid'] = parameters['robotGuid']
  }
  if (parameters['commandId'] !== undefined) {
    queryParameters['commandId'] = parameters['commandId']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogRobotCommand
 * url: AddLogRobotCommandURL
 * method: AddLogRobotCommand_TYPE
 * raw_url: AddLogRobotCommand_RAW_URL
 * @param request - 
 */
export const AddLogRobotCommand = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogRobotCommand'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogRobotCommand_RAW_URL = function() {
  return '/api/AddLogRobotCommand'
}
export const AddLogRobotCommand_TYPE = function() {
  return 'post'
}
export const AddLogRobotCommandURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogRobotCommand'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: DeleteLogRobotCommand
 * url: DeleteLogRobotCommandURL
 * method: DeleteLogRobotCommand_TYPE
 * raw_url: DeleteLogRobotCommand_RAW_URL
 * @param id - 
 */
export const DeleteLogRobotCommand = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/DeleteLogRobotCommand'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const DeleteLogRobotCommand_RAW_URL = function() {
  return '/api/DeleteLogRobotCommand'
}
export const DeleteLogRobotCommand_TYPE = function() {
  return 'delete'
}
export const DeleteLogRobotCommandURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/DeleteLogRobotCommand'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogUsers
 * url: GetLogUsersURL
 * method: GetLogUsers_TYPE
 * raw_url: GetLogUsers_RAW_URL
 * @param logTypeId - 
 * @param statusId - 
 * @param userName - 
 * @param statusDesc - 
 * @param logTypeDesc - 
 * @param message - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetLogUsers = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogUsers'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['statusId'] !== undefined) {
    queryParameters['statusId'] = parameters['statusId']
  }
  if (parameters['userName'] !== undefined) {
    queryParameters['userName'] = parameters['userName']
  }
  if (parameters['statusDesc'] !== undefined) {
    queryParameters['statusDesc'] = parameters['statusDesc']
  }
  if (parameters['logTypeDesc'] !== undefined) {
    queryParameters['logTypeDesc'] = parameters['logTypeDesc']
  }
  if (parameters['message'] !== undefined) {
    queryParameters['message'] = parameters['message']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogUsers_RAW_URL = function() {
  return '/api/GetLogUsers'
}
export const GetLogUsers_TYPE = function() {
  return 'get'
}
export const GetLogUsersURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogUsers'
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['statusId'] !== undefined) {
    queryParameters['statusId'] = parameters['statusId']
  }
  if (parameters['userName'] !== undefined) {
    queryParameters['userName'] = parameters['userName']
  }
  if (parameters['statusDesc'] !== undefined) {
    queryParameters['statusDesc'] = parameters['statusDesc']
  }
  if (parameters['logTypeDesc'] !== undefined) {
    queryParameters['logTypeDesc'] = parameters['logTypeDesc']
  }
  if (parameters['message'] !== undefined) {
    queryParameters['message'] = parameters['message']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogUser
 * url: AddLogUserURL
 * method: AddLogUser_TYPE
 * raw_url: AddLogUser_RAW_URL
 * @param request - 
 */
export const AddLogUser = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogUser'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogUser_RAW_URL = function() {
  return '/api/AddLogUser'
}
export const AddLogUser_TYPE = function() {
  return 'post'
}
export const AddLogUserURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogUser'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogActivities
 * url: GetLogActivitiesURL
 * method: GetLogActivities_TYPE
 * raw_url: GetLogActivities_RAW_URL
 * @param activityPage - 
 * @param dateFrom - 
 * @param dateTo - 
 * @param userName - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetLogActivities = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogActivities'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['activityPage'] !== undefined) {
    queryParameters['activityPage'] = parameters['activityPage']
  }
  if (parameters['dateFrom'] !== undefined) {
    queryParameters['dateFrom'] = parameters['dateFrom']
  }
  if (parameters['dateTo'] !== undefined) {
    queryParameters['dateTo'] = parameters['dateTo']
  }
  if (parameters['userName'] !== undefined) {
    queryParameters['userName'] = parameters['userName']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogActivities_RAW_URL = function() {
  return '/api/GetLogActivities'
}
export const GetLogActivities_TYPE = function() {
  return 'get'
}
export const GetLogActivitiesURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogActivities'
  if (parameters['activityPage'] !== undefined) {
    queryParameters['activityPage'] = parameters['activityPage']
  }
  if (parameters['dateFrom'] !== undefined) {
    queryParameters['dateFrom'] = parameters['dateFrom']
  }
  if (parameters['dateTo'] !== undefined) {
    queryParameters['dateTo'] = parameters['dateTo']
  }
  if (parameters['userName'] !== undefined) {
    queryParameters['userName'] = parameters['userName']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogRobotHealthCheck
 * url: AddLogRobotHealthCheckURL
 * method: AddLogRobotHealthCheck_TYPE
 * raw_url: AddLogRobotHealthCheck_RAW_URL
 * @param id - 
 */
export const AddLogRobotHealthCheck = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogRobotHealthCheck'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogRobotHealthCheck_RAW_URL = function() {
  return '/api/AddLogRobotHealthCheck'
}
export const AddLogRobotHealthCheck_TYPE = function() {
  return 'post'
}
export const AddLogRobotHealthCheckURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogRobotHealthCheck'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogRobotHealthCheckV2
 * url: AddLogRobotHealthCheckV2URL
 * method: AddLogRobotHealthCheckV2_TYPE
 * raw_url: AddLogRobotHealthCheckV2_RAW_URL
 * @param id - 
 * @param request - 
 */
export const AddLogRobotHealthCheckV2 = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/v2/AddLogRobotHealthCheck'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogRobotHealthCheckV2_RAW_URL = function() {
  return '/api/v2/AddLogRobotHealthCheck'
}
export const AddLogRobotHealthCheckV2_TYPE = function() {
  return 'post'
}
export const AddLogRobotHealthCheckV2URL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/v2/AddLogRobotHealthCheck'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogRobotHealtCheckTotalbCountByRobotId
 * url: GetLogRobotHealtCheckTotalbCountByRobotIdURL
 * method: GetLogRobotHealtCheckTotalbCountByRobotId_TYPE
 * raw_url: GetLogRobotHealtCheckTotalbCountByRobotId_RAW_URL
 * @param id - 
 */
export const GetLogRobotHealtCheckTotalbCountByRobotId = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogRobotHealtCheckTotalbCountByRobotId'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogRobotHealtCheckTotalbCountByRobotId_RAW_URL = function() {
  return '/api/GetLogRobotHealtCheckTotalbCountByRobotId'
}
export const GetLogRobotHealtCheckTotalbCountByRobotId_TYPE = function() {
  return 'get'
}
export const GetLogRobotHealtCheckTotalbCountByRobotIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogRobotHealtCheckTotalbCountByRobotId'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogGenLicenseKey
 * url: GetLogGenLicenseKeyURL
 * method: GetLogGenLicenseKey_TYPE
 * raw_url: GetLogGenLicenseKey_RAW_URL
 * @param licenseId - 
 * @param custCode - 
 * @param applicationTd - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetLogGenLicenseKey = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogGenLicenseKey'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['licenseId'] !== undefined) {
    queryParameters['licenseId'] = parameters['licenseId']
  }
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['applicationTd'] !== undefined) {
    queryParameters['applicationTd'] = parameters['applicationTd']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogGenLicenseKey_RAW_URL = function() {
  return '/api/GetLogGenLicenseKey'
}
export const GetLogGenLicenseKey_TYPE = function() {
  return 'get'
}
export const GetLogGenLicenseKeyURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogGenLicenseKey'
  if (parameters['licenseId'] !== undefined) {
    queryParameters['licenseId'] = parameters['licenseId']
  }
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['applicationTd'] !== undefined) {
    queryParameters['applicationTd'] = parameters['applicationTd']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogActivateKey
 * url: GetLogActivateKeyURL
 * method: GetLogActivateKey_TYPE
 * raw_url: GetLogActivateKey_RAW_URL
 * @param custCode - 
 * @param applicationId - 
 * @param keyId - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetLogActivateKey = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogActivateKey'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['applicationId'] !== undefined) {
    queryParameters['applicationId'] = parameters['applicationId']
  }
  if (parameters['keyId'] !== undefined) {
    queryParameters['keyId'] = parameters['keyId']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogActivateKey_RAW_URL = function() {
  return '/api/GetLogActivateKey'
}
export const GetLogActivateKey_TYPE = function() {
  return 'get'
}
export const GetLogActivateKeyURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogActivateKey'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['applicationId'] !== undefined) {
    queryParameters['applicationId'] = parameters['applicationId']
  }
  if (parameters['keyId'] !== undefined) {
    queryParameters['keyId'] = parameters['keyId']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogGenLicenseKey
 * url: AddLogGenLicenseKeyURL
 * method: AddLogGenLicenseKey_TYPE
 * raw_url: AddLogGenLicenseKey_RAW_URL
 * @param request - 
 */
export const AddLogGenLicenseKey = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogGenLicenseKey'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogGenLicenseKey_RAW_URL = function() {
  return '/api/AddLogGenLicenseKey'
}
export const AddLogGenLicenseKey_TYPE = function() {
  return 'post'
}
export const AddLogGenLicenseKeyURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogGenLicenseKey'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogGenLicenseKeys
 * url: AddLogGenLicenseKeysURL
 * method: AddLogGenLicenseKeys_TYPE
 * raw_url: AddLogGenLicenseKeys_RAW_URL
 * @param request - 
 */
export const AddLogGenLicenseKeys = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogGenLicenseKeys'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogGenLicenseKeys_RAW_URL = function() {
  return '/api/AddLogGenLicenseKeys'
}
export const AddLogGenLicenseKeys_TYPE = function() {
  return 'post'
}
export const AddLogGenLicenseKeysURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogGenLicenseKeys'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddLogActivateLicenseKey
 * url: AddLogActivateLicenseKeyURL
 * method: AddLogActivateLicenseKey_TYPE
 * raw_url: AddLogActivateLicenseKey_RAW_URL
 * @param request - 
 */
export const AddLogActivateLicenseKey = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddLogActivateLicenseKey'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddLogActivateLicenseKey_RAW_URL = function() {
  return '/api/AddLogActivateLicenseKey'
}
export const AddLogActivateLicenseKey_TYPE = function() {
  return 'post'
}
export const AddLogActivateLicenseKeyURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddLogActivateLicenseKey'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddOrUpdateJobQueueMessage
 * url: AddOrUpdateJobQueueMessageURL
 * method: AddOrUpdateJobQueueMessage_TYPE
 * raw_url: AddOrUpdateJobQueueMessage_RAW_URL
 * @param request - 
 */
export const AddOrUpdateJobQueueMessage = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddOrUpdateJobQueueMessage'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddOrUpdateJobQueueMessage_RAW_URL = function() {
  return '/api/AddOrUpdateJobQueueMessage'
}
export const AddOrUpdateJobQueueMessage_TYPE = function() {
  return 'post'
}
export const AddOrUpdateJobQueueMessageURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddOrUpdateJobQueueMessage'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetLogActionJobQueueMessageInfo
 * url: GetLogActionJobQueueMessageInfoURL
 * method: GetLogActionJobQueueMessageInfo_TYPE
 * raw_url: GetLogActionJobQueueMessageInfo_RAW_URL
 * @param jobqueueId - 
 * @param scheduleId - 
 * @param logTypeId - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetLogActionJobQueueMessageInfo = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetLogActionJobQueueMessageInfo'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['jobqueueId'] !== undefined) {
    queryParameters['jobqueueId'] = parameters['jobqueueId']
  }
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetLogActionJobQueueMessageInfo_RAW_URL = function() {
  return '/api/GetLogActionJobQueueMessageInfo'
}
export const GetLogActionJobQueueMessageInfo_TYPE = function() {
  return 'get'
}
export const GetLogActionJobQueueMessageInfoURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetLogActionJobQueueMessageInfo'
  if (parameters['jobqueueId'] !== undefined) {
    queryParameters['jobqueueId'] = parameters['jobqueueId']
  }
  if (parameters['scheduleId'] !== undefined) {
    queryParameters['scheduleId'] = parameters['scheduleId']
  }
  if (parameters['logTypeId'] !== undefined) {
    queryParameters['logTypeId'] = parameters['logTypeId']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}