/* eslint-disable */
import axios from 'axios'
import qs from 'qs'
let domain = ''
export const getDomain = () => {
  return domain
}
export const setDomain = ($domain) => {
  domain = $domain
}
export const request = (method, url, body, queryParameters, form, config) => {
  method = method.toLowerCase()
  let keys = Object.keys(queryParameters)
  let queryUrl = url
  if (keys.length > 0) {
    queryUrl = url + '?' + qs.stringify(queryParameters)
  }
  // let queryUrl = url+(keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
  if (body) {
    return axios[method](queryUrl, body, config)
  } else if (method === 'get') {
    return axios[method](queryUrl, config)
  } else {
    return axios[method](queryUrl, qs.stringify(form), config)
  }
}
/*==========================================================
 *                    
 ==========================================================*/
/**
 * 
 * request: GetUserById
 * url: GetUserByIdURL
 * method: GetUserById_TYPE
 * raw_url: GetUserById_RAW_URL
 * @param id - 
 */
export const GetUserById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetUserById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetUserById_RAW_URL = function() {
  return '/api/GetUserById'
}
export const GetUserById_TYPE = function() {
  return 'get'
}
export const GetUserByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetUserById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetUsers
 * url: GetUsersURL
 * method: GetUsers_TYPE
 * raw_url: GetUsers_RAW_URL
 * @param custCode - 
 * @param userName - 
 * @param email - 
 * @param firstName - 
 * @param lastName - 
 * @param isActive - 
 * @param isTemplate - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetUsers = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetUsers'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['userName'] !== undefined) {
    queryParameters['userName'] = parameters['userName']
  }
  if (parameters['email'] !== undefined) {
    queryParameters['email'] = parameters['email']
  }
  if (parameters['firstName'] !== undefined) {
    queryParameters['firstName'] = parameters['firstName']
  }
  if (parameters['lastName'] !== undefined) {
    queryParameters['lastName'] = parameters['lastName']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['isTemplate'] !== undefined) {
    queryParameters['isTemplate'] = parameters['isTemplate']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetUsers_RAW_URL = function() {
  return '/api/GetUsers'
}
export const GetUsers_TYPE = function() {
  return 'get'
}
export const GetUsersURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetUsers'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['userName'] !== undefined) {
    queryParameters['userName'] = parameters['userName']
  }
  if (parameters['email'] !== undefined) {
    queryParameters['email'] = parameters['email']
  }
  if (parameters['firstName'] !== undefined) {
    queryParameters['firstName'] = parameters['firstName']
  }
  if (parameters['lastName'] !== undefined) {
    queryParameters['lastName'] = parameters['lastName']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['isTemplate'] !== undefined) {
    queryParameters['isTemplate'] = parameters['isTemplate']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddOrUpdateUser
 * url: AddOrUpdateUserURL
 * method: AddOrUpdateUser_TYPE
 * raw_url: AddOrUpdateUser_RAW_URL
 * @param request - 
 */
export const AddOrUpdateUser = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddOrUpdateUser'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddOrUpdateUser_RAW_URL = function() {
  return '/api/AddOrUpdateUser'
}
export const AddOrUpdateUser_TYPE = function() {
  return 'post'
}
export const AddOrUpdateUserURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddOrUpdateUser'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetHistoryById
 * url: GetHistoryByIdURL
 * method: GetHistoryById_TYPE
 * raw_url: GetHistoryById_RAW_URL
 * @param id - 
 */
export const GetHistoryById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetHistoryById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetHistoryById_RAW_URL = function() {
  return '/api/GetHistoryById'
}
export const GetHistoryById_TYPE = function() {
  return 'get'
}
export const GetHistoryByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetHistoryById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: ForgotPasswordByEmail
 * url: ForgotPasswordByEmailURL
 * method: ForgotPasswordByEmail_TYPE
 * raw_url: ForgotPasswordByEmail_RAW_URL
 * @param email - 
 */
export const ForgotPasswordByEmail = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/ForgotPasswordByEmail'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['email'] !== undefined) {
    queryParameters['email'] = parameters['email']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const ForgotPasswordByEmail_RAW_URL = function() {
  return '/api/ForgotPasswordByEmail'
}
export const ForgotPasswordByEmail_TYPE = function() {
  return 'get'
}
export const ForgotPasswordByEmailURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/ForgotPasswordByEmail'
  if (parameters['email'] !== undefined) {
    queryParameters['email'] = parameters['email']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: DeleteUser
 * url: DeleteUserURL
 * method: DeleteUser_TYPE
 * raw_url: DeleteUser_RAW_URL
 * @param id - 
 */
export const DeleteUser = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/DeleteUser'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const DeleteUser_RAW_URL = function() {
  return '/api/DeleteUser'
}
export const DeleteUser_TYPE = function() {
  return 'delete'
}
export const DeleteUserURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/DeleteUser'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: ChangeUserPassword
 * url: ChangeUserPasswordURL
 * method: ChangeUserPassword_TYPE
 * raw_url: ChangeUserPassword_RAW_URL
 * @param request - 
 */
export const ChangeUserPassword = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/ChangeUserPassword'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('patch', domain + path, body, queryParameters, form, config)
}
export const ChangeUserPassword_RAW_URL = function() {
  return '/api/ChangeUserPassword'
}
export const ChangeUserPassword_TYPE = function() {
  return 'patch'
}
export const ChangeUserPasswordURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/ChangeUserPassword'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetDepartmentById
 * url: GetDepartmentByIdURL
 * method: GetDepartmentById_TYPE
 * raw_url: GetDepartmentById_RAW_URL
 * @param id - 
 */
export const GetDepartmentById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetDepartmentById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetDepartmentById_RAW_URL = function() {
  return '/api/GetDepartmentById'
}
export const GetDepartmentById_TYPE = function() {
  return 'get'
}
export const GetDepartmentByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetDepartmentById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetDepartments
 * url: GetDepartmentsURL
 * method: GetDepartments_TYPE
 * raw_url: GetDepartments_RAW_URL
 * @param custCode - 
 * @param deptCode - 
 * @param deptName - 
 * @param deptDesc - 
 * @param isActive - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetDepartments = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetDepartments'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['deptCode'] !== undefined) {
    queryParameters['deptCode'] = parameters['deptCode']
  }
  if (parameters['deptName'] !== undefined) {
    queryParameters['deptName'] = parameters['deptName']
  }
  if (parameters['deptDesc'] !== undefined) {
    queryParameters['deptDesc'] = parameters['deptDesc']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetDepartments_RAW_URL = function() {
  return '/api/GetDepartments'
}
export const GetDepartments_TYPE = function() {
  return 'get'
}
export const GetDepartmentsURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetDepartments'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['deptCode'] !== undefined) {
    queryParameters['deptCode'] = parameters['deptCode']
  }
  if (parameters['deptName'] !== undefined) {
    queryParameters['deptName'] = parameters['deptName']
  }
  if (parameters['deptDesc'] !== undefined) {
    queryParameters['deptDesc'] = parameters['deptDesc']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddOrUpdateDepartment
 * url: AddOrUpdateDepartmentURL
 * method: AddOrUpdateDepartment_TYPE
 * raw_url: AddOrUpdateDepartment_RAW_URL
 * @param request - 
 */
export const AddOrUpdateDepartment = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddOrUpdateDepartment'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddOrUpdateDepartment_RAW_URL = function() {
  return '/api/AddOrUpdateDepartment'
}
export const AddOrUpdateDepartment_TYPE = function() {
  return 'post'
}
export const AddOrUpdateDepartmentURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddOrUpdateDepartment'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: DeleteDepartment
 * url: DeleteDepartmentURL
 * method: DeleteDepartment_TYPE
 * raw_url: DeleteDepartment_RAW_URL
 * @param id - 
 */
export const DeleteDepartment = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/DeleteDepartment'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const DeleteDepartment_RAW_URL = function() {
  return '/api/DeleteDepartment'
}
export const DeleteDepartment_TYPE = function() {
  return 'delete'
}
export const DeleteDepartmentURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/DeleteDepartment'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: CheckLicenseByCusCode
 * url: CheckLicenseByCusCodeURL
 * method: CheckLicenseByCusCode_TYPE
 * raw_url: CheckLicenseByCusCode_RAW_URL
 * @param cusCode - 
 */
export const CheckLicenseByCusCode = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/CheckLicenseByCusCode'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['cusCode'] !== undefined) {
    queryParameters['cusCode'] = parameters['cusCode']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const CheckLicenseByCusCode_RAW_URL = function() {
  return '/api/CheckLicenseByCusCode'
}
export const CheckLicenseByCusCode_TYPE = function() {
  return 'get'
}
export const CheckLicenseByCusCodeURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/CheckLicenseByCusCode'
  if (parameters['cusCode'] !== undefined) {
    queryParameters['cusCode'] = parameters['cusCode']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetCustomerById
 * url: GetCustomerByIdURL
 * method: GetCustomerById_TYPE
 * raw_url: GetCustomerById_RAW_URL
 * @param id - 
 */
export const GetCustomerById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetCustomerById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetCustomerById_RAW_URL = function() {
  return '/api/GetCustomerById'
}
export const GetCustomerById_TYPE = function() {
  return 'get'
}
export const GetCustomerByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetCustomerById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetCustomer
 * url: GetCustomerURL
 * method: GetCustomer_TYPE
 * raw_url: GetCustomer_RAW_URL
 * @param custCode - 
 * @param customerName - 
 * @param isActive - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetCustomer = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetCustomer'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['customerName'] !== undefined) {
    queryParameters['customerName'] = parameters['customerName']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetCustomer_RAW_URL = function() {
  return '/api/GetCustomer'
}
export const GetCustomer_TYPE = function() {
  return 'get'
}
export const GetCustomerURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetCustomer'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['customerName'] !== undefined) {
    queryParameters['customerName'] = parameters['customerName']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddOrUpdateCustomer
 * url: AddOrUpdateCustomerURL
 * method: AddOrUpdateCustomer_TYPE
 * raw_url: AddOrUpdateCustomer_RAW_URL
 * @param request - 
 */
export const AddOrUpdateCustomer = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddOrUpdateCustomer'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddOrUpdateCustomer_RAW_URL = function() {
  return '/api/AddOrUpdateCustomer'
}
export const AddOrUpdateCustomer_TYPE = function() {
  return 'post'
}
export const AddOrUpdateCustomerURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddOrUpdateCustomer'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}