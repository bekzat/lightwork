/* eslint-disable */
import axios from 'axios'
import qs from 'qs'
let domain = ''
export const getDomain = () => {
  return domain
}
export const setDomain = ($domain) => {
  domain = $domain
}
export const request = (method, url, body, queryParameters, form, config) => {
  method = method.toLowerCase()
  let keys = Object.keys(queryParameters)
  let queryUrl = url
  if (keys.length > 0) {
    queryUrl = url + '?' + qs.stringify(queryParameters)
  }
  // let queryUrl = url+(keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
  if (body) {
    return axios[method](queryUrl, body, config)
  } else if (method === 'get') {
    return axios[method](queryUrl, config)
  } else {
    return axios[method](queryUrl, qs.stringify(form), config)
  }
}
/*==========================================================
 *                    
 ==========================================================*/
/**
 * 
 * request: GetById
 * url: GetByIdURL
 * method: GetById_TYPE
 * raw_url: GetById_RAW_URL
 * @param id - 
 */
export const GetById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetById_RAW_URL = function() {
  return '/api/GetById'
}
export const GetById_TYPE = function() {
  return 'get'
}
export const GetByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetRobotListByGuid
 * url: GetRobotListByGuidURL
 * method: GetRobotListByGuid_TYPE
 * raw_url: GetRobotListByGuid_RAW_URL
 * @param request - 
 */
export const GetRobotListByGuid = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetRobotListByGuid'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const GetRobotListByGuid_RAW_URL = function() {
  return '/api/GetRobotListByGuid'
}
export const GetRobotListByGuid_TYPE = function() {
  return 'post'
}
export const GetRobotListByGuidURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetRobotListByGuid'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetAllCommanderRobots
 * url: GetAllCommanderRobotsURL
 * method: GetAllCommanderRobots_TYPE
 * raw_url: GetAllCommanderRobots_RAW_URL
 */
export const GetAllCommanderRobots = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetAllCommanderRobots'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetAllCommanderRobots_RAW_URL = function() {
  return '/api/GetAllCommanderRobots'
}
export const GetAllCommanderRobots_TYPE = function() {
  return 'get'
}
export const GetAllCommanderRobotsURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetAllCommanderRobots'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetRobots
 * url: GetRobotsURL
 * method: GetRobots_TYPE
 * raw_url: GetRobots_RAW_URL
 * @param robotName - 
 * @param robotGuid - 
 * @param isActive - 
 * @param robotStatus - 
 * @param isCommander - 
 * @param orderBy - 
 * @param pageNo - 
 * @param pageSize - 
 * @param isPagination - 
 */
export const GetRobots = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetRobots'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['robotGuid'] !== undefined) {
    queryParameters['robotGuid'] = parameters['robotGuid']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['robotStatus'] !== undefined) {
    queryParameters['robotStatus'] = parameters['robotStatus']
  }
  if (parameters['isCommander'] !== undefined) {
    queryParameters['isCommander'] = parameters['isCommander']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetRobots_RAW_URL = function() {
  return '/api/GetRobots'
}
export const GetRobots_TYPE = function() {
  return 'get'
}
export const GetRobotsURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetRobots'
  if (parameters['robotName'] !== undefined) {
    queryParameters['robotName'] = parameters['robotName']
  }
  if (parameters['robotGuid'] !== undefined) {
    queryParameters['robotGuid'] = parameters['robotGuid']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters['robotStatus'] !== undefined) {
    queryParameters['robotStatus'] = parameters['robotStatus']
  }
  if (parameters['isCommander'] !== undefined) {
    queryParameters['isCommander'] = parameters['isCommander']
  }
  if (parameters['orderBy'] !== undefined) {
    queryParameters['orderBy'] = parameters['orderBy']
  }
  if (parameters['pageNo'] !== undefined) {
    queryParameters['pageNo'] = parameters['pageNo']
  }
  if (parameters['pageSize'] !== undefined) {
    queryParameters['pageSize'] = parameters['pageSize']
  }
  if (parameters['isPagination'] !== undefined) {
    queryParameters['isPagination'] = parameters['isPagination']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: AddOrUpdateRobot
 * url: AddOrUpdateRobotURL
 * method: AddOrUpdateRobot_TYPE
 * raw_url: AddOrUpdateRobot_RAW_URL
 * @param request - 
 */
export const AddOrUpdateRobot = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/AddOrUpdateRobot'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const AddOrUpdateRobot_RAW_URL = function() {
  return '/api/AddOrUpdateRobot'
}
export const AddOrUpdateRobot_TYPE = function() {
  return 'post'
}
export const AddOrUpdateRobotURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/AddOrUpdateRobot'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: UpdateRobotByGuid
 * url: UpdateRobotByGuidURL
 * method: UpdateRobotByGuid_TYPE
 * raw_url: UpdateRobotByGuid_RAW_URL
 * @param request - 
 */
export const UpdateRobotByGuid = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/UpdateRobotByGuid'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const UpdateRobotByGuid_RAW_URL = function() {
  return '/api/UpdateRobotByGuid'
}
export const UpdateRobotByGuid_TYPE = function() {
  return 'post'
}
export const UpdateRobotByGuidURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/UpdateRobotByGuid'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: UpdateRobotTotalRunning
 * url: UpdateRobotTotalRunningURL
 * method: UpdateRobotTotalRunning_TYPE
 * raw_url: UpdateRobotTotalRunning_RAW_URL
 * @param request - 
 */
export const UpdateRobotTotalRunning = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/UpdateRobotTotalRunning'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const UpdateRobotTotalRunning_RAW_URL = function() {
  return '/api/UpdateRobotTotalRunning'
}
export const UpdateRobotTotalRunning_TYPE = function() {
  return 'post'
}
export const UpdateRobotTotalRunningURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/UpdateRobotTotalRunning'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: DeleteRobot
 * url: DeleteRobotURL
 * method: DeleteRobot_TYPE
 * raw_url: DeleteRobot_RAW_URL
 * @param id - 
 */
export const DeleteRobot = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/DeleteRobot'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('delete', domain + path, body, queryParameters, form, config)
}
export const DeleteRobot_RAW_URL = function() {
  return '/api/DeleteRobot'
}
export const DeleteRobot_TYPE = function() {
  return 'delete'
}
export const DeleteRobotURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/DeleteRobot'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: GetHistoryById
 * url: GetHistoryByIdURL
 * method: GetHistoryById_TYPE
 * raw_url: GetHistoryById_RAW_URL
 * @param id - 
 */
export const GetHistoryById = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/GetHistoryById'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const GetHistoryById_RAW_URL = function() {
  return '/api/GetHistoryById'
}
export const GetHistoryById_TYPE = function() {
  return 'get'
}
export const GetHistoryByIdURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/GetHistoryById'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: SetStatusRobot
 * url: SetStatusRobotURL
 * method: SetStatusRobot_TYPE
 * raw_url: SetStatusRobot_RAW_URL
 * @param custCode - 
 * @param isActive - 
 */
export const SetStatusRobot = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/SetStatusRobot'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('patch', domain + path, body, queryParameters, form, config)
}
export const SetStatusRobot_RAW_URL = function() {
  return '/api/SetStatusRobot'
}
export const SetStatusRobot_TYPE = function() {
  return 'patch'
}
export const SetStatusRobotURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/SetStatusRobot'
  if (parameters['custCode'] !== undefined) {
    queryParameters['custCode'] = parameters['custCode']
  }
  if (parameters['isActive'] !== undefined) {
    queryParameters['isActive'] = parameters['isActive']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: CheckRobotActive
 * url: CheckRobotActiveURL
 * method: CheckRobotActive_TYPE
 * raw_url: CheckRobotActive_RAW_URL
 * @param id - 
 */
export const CheckRobotActive = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/CheckRobotActive'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('get', domain + path, body, queryParameters, form, config)
}
export const CheckRobotActive_RAW_URL = function() {
  return '/api/CheckRobotActive'
}
export const CheckRobotActive_TYPE = function() {
  return 'get'
}
export const CheckRobotActiveURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/CheckRobotActive'
  if (parameters['id'] !== undefined) {
    queryParameters['id'] = parameters['id']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: StartRobot
 * url: StartRobotURL
 * method: StartRobot_TYPE
 * raw_url: StartRobot_RAW_URL
 * @param request - 
 */
export const StartRobot = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/StartRobot'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const StartRobot_RAW_URL = function() {
  return '/api/StartRobot'
}
export const StartRobot_TYPE = function() {
  return 'post'
}
export const StartRobotURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/StartRobot'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: CloseRobot
 * url: CloseRobotURL
 * method: CloseRobot_TYPE
 * raw_url: CloseRobot_RAW_URL
 * @param request - 
 */
export const CloseRobot = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/CloseRobot'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('post', domain + path, body, queryParameters, form, config)
}
export const CloseRobot_RAW_URL = function() {
  return '/api/CloseRobot'
}
export const CloseRobot_TYPE = function() {
  return 'post'
}
export const CloseRobotURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/CloseRobot'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}
/**
 * 
 * request: UpdateRobotCommandStatus
 * url: UpdateRobotCommandStatusURL
 * method: UpdateRobotCommandStatus_TYPE
 * raw_url: UpdateRobotCommandStatus_RAW_URL
 * @param request - 
 */
export const UpdateRobotCommandStatus = function(parameters = {}) {
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  const config = parameters.$config
  let path = '/api/UpdateRobotCommandStatus'
  let body
  let queryParameters = {}
  let form = {}
  if (parameters['request'] !== undefined) {
    body = parameters['request']
  }
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    });
  }
  return request('patch', domain + path, body, queryParameters, form, config)
}
export const UpdateRobotCommandStatus_RAW_URL = function() {
  return '/api/UpdateRobotCommandStatus'
}
export const UpdateRobotCommandStatus_TYPE = function() {
  return 'patch'
}
export const UpdateRobotCommandStatusURL = function(parameters = {}) {
  let queryParameters = {}
  const domain = parameters.$domain ? parameters.$domain : getDomain()
  let path = '/api/UpdateRobotCommandStatus'
  if (parameters.$queryParameters) {
    Object.keys(parameters.$queryParameters).forEach(function(parameterName) {
      queryParameters[parameterName] = parameters.$queryParameters[parameterName]
    })
  }
  let keys = Object.keys(queryParameters)
  return domain + path + (keys.length > 0 ? '?' + (keys.map(key => key + '=' + encodeURIComponent(queryParameters[key])).join('&')) : '')
}