const state = () => {
  return {
    menu: [
      {
        icon: 'GridIcon',
        pageName: 'side-menu-dashboard',
        title: 'Dashboard'
      },
      {
        icon: 'ClockIcon',
        pageName: 'side-menu-queue',
        title: 'Queue',
        subMenu: [
          {
            icon: '',
            pageName: 'side-menu-queue-runs',
            title: 'Upcoming Runs'
          },
          {
            icon: '',
            pageName: 'side-menu-queue-history',
            title: 'History'
          },
          {
            icon: '',
            pageName: 'side-menu-queue-calendar',
            title: 'Queue Calendar'
          },
          {
            icon: '',
            pageName: 'side-menu-queue-log',
            title: 'Operation Log'
          }
        ]
      },
      {
        icon: 'RobotIcon',
        pageName: 'side-menu-robots',
        title: 'Robots',
        subMenu: [
          {
            icon: '',
            pageName: 'side-menu-robots-all',
            title: 'All Robots'
          },
          {
            icon: '',
            pageName: 'side-menu-robots-deployment',
            title: 'Deployment'
          }
        ]
      },
      {
        icon: 'BoxIcon',
        pageName: 'side-menu-processes',
        title: 'Processes',
        subMenu: [
          {
            icon: '',
            pageName: 'side-menu-processes-all',
            title: 'All Processes'
          },
          {
            icon: '',
            pageName: 'side-menu-process-log',
            title: 'Process Logs'
          }
        ]
      },
      {
        icon: 'ChainIcon',
        pageName: 'side-menu-groups',
        title: 'Process Groups',
        subMenu: [
          {
            icon: '',
            pageName: 'side-menu-groups-all',
            title: 'All Process Groups'
          },
          {
            icon: '',
            pageName: 'side-menu-groups-log',
            title: 'Process Group Logs'
          }
        ]
      },
      {
        icon: 'UsersIcon',
        pageName: 'side-menu-users',
        title: 'User Management',
        subMenu: [
          {
            icon: '',
            pageName: 'side-menu-users-all',
            title: 'Users'
          },
          {
            icon: '',
            pageName: 'side-menu-users-departments',
            title: 'Departments'
          },
          {
            icon: '',
            pageName: 'side-menu-users-roles',
            title: 'Roles & Permissions'
          },
          {
            icon: '',
            pageName: 'side-menu-users-activity',
            title: 'Activity Log'
          },
          {
            icon: '',
            pageName: 'side-menu-users-login-history',
            title: 'Login History'
          }
        ]
      },
      {
        icon: 'LayersIcon',
        pageName: 'side-menu-workflow-storage',
        title: 'Workflow Storage'
      },
      'devider',
      {
        icon: 'SettingsIcon',
        pageName: 'side-menu-settings',
        title: 'Settings',
        subMenu: [
          {
            icon: '',
            pageName: 'side-menu-settings-nwd',
            title: 'Non-Working Days'
          },
          {
            icon: '',
            pageName: 'side-menu-settings-smtp',
            title: 'SMTP Configuration'
          },
          {
            icon: '',
            pageName: 'side-menu-settings-database-password',
            title: 'Database Password'
          },
          {
            icon: '',
            pageName: 'side-menu-settings-license',
            title: 'License'
          },
          {
            icon: '',
            pageName: 'side-menu-settings-other',
            title: 'Other'
          }
        ]
      },
      'devider',
      'devider',
      'devider',
      'devider',
      'devider',
      {
        icon: 'LayoutIcon',
        pageName: 'side-menu-layout',
        title: 'Assets (delete later)',
        subMenu: [
          {
            icon: '',
            pageName: 'login',
            title: 'Login'
          },
          {
            icon: '',
            pageName: 'register',
            title: 'Register'
          },
          {
            icon: '',
            pageName: 'error-page',
            title: 'Error Page'
          },
          {
            icon: '',
            pageName: 'side-menu-change-password',
            title: 'Change Password'
          },
          {
            icon: '',
            pageName: 'side-menu-table',
            title: 'Table',
            subMenu: [
              {
                icon: '',
                pageName: 'side-menu-regular-table',
                title: 'Regular Table'
              },
              {
                icon: '',
                pageName: 'side-menu-tabulator',
                title: 'Tabulator'
              }
            ]
          },
          {
            icon: '',
            pageName: 'side-menu-overlay',
            title: 'Overlay',
            subMenu: [
              {
                icon: '',
                pageName: 'side-menu-modal',
                title: 'Modal'
              },
              {
                icon: '',
                pageName: 'side-menu-slide-over',
                title: 'Slide Over'
              }
            ]
          },
          {
            icon: '',
            pageName: 'side-menu-button',
            title: 'Button'
          },
          {
            icon: '',
            pageName: 'side-menu-alert',
            title: 'Alert'
          },
          {
            icon: '',
            pageName: 'side-menu-tooltip',
            title: 'Tooltip'
          },
          {
            icon: '',
            pageName: 'side-menu-dropdown',
            title: 'Dropdown'
          },
          {
            icon: '',
            pageName: 'side-menu-regular-form',
            title: 'Regular Form'
          },
          {
            icon: '',
            pageName: 'side-menu-datepicker',
            title: 'Datepicker'
          },
          {
            icon: '',
            pageName: 'side-menu-tom-select',
            title: 'Tom Select'
          },
          {
            icon: '',
            pageName: 'side-menu-file-upload',
            title: 'File Upload'
          },
          {
            icon: '',
            pageName: 'side-menu-validation',
            title: 'Validation'
          }
        ]
      }
    ]
  }
}

// getters
const getters = {
  menu: state => state.menu
}

// actions
const actions = {}

// mutations
const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
