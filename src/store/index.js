import { createStore } from 'vuex'
import main from './main'
import sideMenu from './side-menu'
import userStore from '@/store/modules/user'

const store = createStore({
  modules: {
    main,
    sideMenu,
    user: userStore
  }
})

export function useStore() {
  return store
}

export default store
