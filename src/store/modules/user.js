import { GetUsers } from '@/api/user'

const config = {
  baseURL: 'https://dev.lightworktech.com/User',
  headers: {
    Authorization:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InV4dWkiLCJDdXN0b21lckNvZGUiOiJJSSIsIlVzZXJJZCI6IjY2IiwiVXNlclR5cGUiOiIzIiwibmJmIjoxNjQxNTQwNjI3LCJleHAiOjE2NDE3OTk4MjcsImlhdCI6MTY0MTU0MDYyN30.huWZ077FRI-Sp2CIlcZlY8wBPK2Jo3GoTtFRejbdfCY'
  }
}
const userStore = {
  state: () => ({
    userConnection: {
      totalRecordCount: 0,
      users: []
    }
  }),
  mutations: {
    setUsers(state, data) {
      console.log('data', data)
      state.userConnection = data
    }
  },
  actions: {
    fetchUsers(context) {
      GetUsers({
        custCode: 'II',
        $config: config
      }).then(res => {
        context.commit('setUsers', res.data.data)
      })
    }
  },
  getters: {
    users(state) {
      return state.userConnection.users
    }
  }
}

export default userStore
