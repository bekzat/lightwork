import { GetRobots } from '@/api/robot'

const config = {
  baseURL: 'https://dev.lightworktech.com/User',
  headers: {
    Authorization:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6InV4dWkiLCJDdXN0b21lckNvZGUiOiJJSSIsIlVzZXJJZCI6IjY2IiwiVXNlclR5cGUiOiIzIiwibmJmIjoxNjQxNTQwNjI3LCJleHAiOjE2NDE3OTk4MjcsImlhdCI6MTY0MTU0MDYyN30.huWZ077FRI-Sp2CIlcZlY8wBPK2Jo3GoTtFRejbdfCY'
  }
}

const robotStore = {
  state: () => ({
    robots: []
  }),
  mutations: {
    setRobots(state, robots) {
      state.robots = robots
    }
  },
  actions: {
    fetchRobots(store) {
      GetRobots({
        $config: config
      }).then(res => {
        console.log('res', res)
      })
    }
  },
  getters: {
    robots(state) {
      return state.robots
    }
  }
}

export default robotStore
