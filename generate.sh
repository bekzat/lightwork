ROOT=$(pwd)
echo "$ROOT"

curl --request GET -sL \
     --url 'https://dev.lightworktech.com/User/docs/v1/swagger.json'\
     --output "$ROOT/src/api/user.json"

curl --request GET -sL \
     --url 'https://dev.lightworktech.com/Robot/docs/v1/swagger.json'\
     --output "$ROOT/src/api/robot.json"
curl --request GET -sL \
     --url 'https://dev.lightworktech.com/Schedule/docs/v1/swagger.json'\
     --output "$ROOT/src/api/schedule.json"
curl --request GET -sL \
     --url 'https://dev.lightworktech.com/Process/docs/v1/swagger.json'\
     --output "$ROOT/src/api/process.json"
curl --request GET -sL \
     --url 'https://dev.lightworktech.com/JobQueue/docs/v1/swagger.json'\
     --output "$ROOT/src/api/job_queue.json"
curl --request GET -sL \
     --url 'https://dev.lightworktech.com/Log/docs/v1/swagger.json'\
     --output "$ROOT/src/api/logs.json"

node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/user.json" -d "$ROOT/src/api/user.js" -m robot
node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/robot.json" -d "$ROOT/src/api/robot.js" -m robot
node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/schedule.json" -d "$ROOT/src/api/schedule.js" -m schedule
node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/process.json" -d "$ROOT/src/api/process.js" -m process
node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/job_queue.json" -d "$ROOT/src/api/job_queue.js" -m jobQueue
node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/logs.json" -d "$ROOT/src/api/logs.js" -m logs


#
#npx @openapitools/openapi-generator-cli generate -i "$ROOT/src/api/user.json" -g javascript --additional-properties=useES6=true,usePromises=true -o "$ROOT/src/api/user"
#npx @openapitools/openapi-generator-cli generate -i "$ROOT/src/api/robot.json" -g javascript --additional-properties=useES6=true,npmVersion=6.9.0,usePromises=true -o "$ROOT/src/api/robot"
#npx @openapitools/openapi-generator-cli generate -i "$ROOT/src/api/job_queue.json" -g javascript --additional-properties=useES6=true,npmVersion=6.9.0,usePromises=true -o "$ROOT/src/api/job_queue"
#npx @openapitools/openapi-generator-cli generate -i "$ROOT/src/api/process.json" -g javascript --additional-properties=useES6=true,npmVersion=6.9.0,usePromises=true -o "$ROOT/src/api/process"
#npx @openapitools/openapi-generator-cli generate -i "$ROOT/src/api/schedule.json" -g javascript --additional-properties=useES6=true,npmVersion=6.9.0,usePromises=true -o "$ROOT/src/api/schedule"
#npx @openapitools/openapi-generator-cli generate -i "$ROOT/src/api/logs.json" -g javascript --additional-properties=useES6=true,npmVersion=6.9.0,usePromises=true -o "$ROOT/src/api/logs"


#node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/robot.json" -d "$ROOT/src/api/robot.js" -m robot
#node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/schedule.json" -d "$ROOT/src/api/schedule.js" -m schedule
#node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/process.json" -d "$ROOT/src/api/process.js" -m process
#node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/job_queue.json" -d "$ROOT/src/api/job_queue.js" -m jobQueue
#node ./node_modules/swagger-vue-generator/bin/generate-api.js -s "$ROOT/src/api/logs.json" -d "$ROOT/src/api/logs.js" -m logs
